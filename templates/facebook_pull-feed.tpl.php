<?php
/**
 * @file
 * Default theme implementation of a Facebook Feed.
 *
 * Available variables:
 * - items: Array of posts.
 */
?>
<ul class="facebook-feed">
<?php foreach ($items as $item): ?>
  <li class="item">
  
    <span class="facebook-feed-picture">
      <img alt="<?php echo $item->from->name; ?>" src="//graph.facebook.com/<?php echo $item->from->id; ?>/picture" />
    </span>
    
    <span class="facebook-feed-from">
      <a href="//facebook.com/profile.php?id=<?php echo $item->from->id; ?>"><?php echo $item->from->name; ?></a>
    </span>
    
    <?php if (isset($item->story)): ?>
      <span class="facebook-feed-story">
        <?php echo str_replace($item->from->name, '', $item->story); ?>
      </span>
    <?php endif; ?>
    
    <span class="facebook-feed-message">
      <?php 
      if (isset($item->message)) {
        echo $item->message;
      }
        if (($item->type === 'link' || $item->type === 'event') && isset($item->name) && isset($item->link)) {
          echo ' ' . l($item->name, $item->link);
        }
        if (in_array($item->type, array('photo', 'video')) && isset($item->picture) && isset($item->link)) {
          //echo l('<img src="' . $item->picture . '" />', $item->link, array('html' => true, 'attributes' => array('target' => '_blank')));
        }
        if ($item->type === 'question') {
          echo $item->question;
          foreach ($item->options as $option) {
            echo '<span>' . $option . '</span>';
          }
        }
      ?>
    </span>
    
    <span class="facebook-feed-time">
      <?php echo t('!time ago.', array('!time' => format_interval(time() - strtotime($item->created_time)))); ?>
    </span>
    <span class="facebook-more-link">
      <?php echo ' ' . l("View Post >>", $item->link); ?>
    <span>
  
  </li>
<?php endforeach; ?>
</ul>
